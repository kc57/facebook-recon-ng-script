import framework
# unique to module
import requests
import json
import sys
from urlparse import urlparse

class Module(framework.module):

    SEARCH_URL = 'https://graph.facebook.com/search?q=%%27%(query)s%%27&type=user&access_token=%(token)s'
    GRAPH_URL = 'https://graph.facebook.com/'
    
    # Because these are short-lived tokens, we can't manage them with the built in API key management
    access_token = ''
    contacts = [[]]

    def __init__(self, params):
        framework.module.__init__(self, params)
        #self.register_option('query', '', 'no', 'The query to search for')
        self.register_option('phone', '', 'yes', 'The phone number to search for')
        self.register_option('token', '', 'yes', 'Your Facebook access token')
        self.register_option('verbose', self.goptions['verbose']['value'], 'yes', self.goptions['verbose']['desc'])
        self.info = {
                     'Name': 'Facebook Harvester',
                     'Author': 'Rob Simon (@_Kc57)',
                     'Description': 'Attempts to match phone numbers to Facebook accounts and pulls any public data for each match.',
                     'Comments': [
                                    'The best use of this module is to brute force a range of phone numbers and discover associated',
                                    'accounts. Brute forcing is supported by using asterisk symbols in place of numbers',
                                    'For example: set phone 555-555-**** will search 5555550000 to 5555559999',
                                    'Since Facebook does not currently support the concept of an API token, you must sign up for a',
                                    'developer account and use the short-lived access token to perform a search.'
                     ]
                     }

    def do_run(self, params):
        if not self.validate_options(): return
        # === begin here ===
        self.contacts = [['FB ID','Phone','Username','First','Last','Gender','URL']]
        self.access_token = self.options['token']['value']
        phone = self.options['phone']['value']
        # Force the variable to a string in case the user enters only digits
        if self.phoneSearch(str(phone)):
            self.table(self.contacts,True)
        
    def search(self, query):
        #query = self.options['query']['value']
        id = self.searchFirstResult(query)
        if id is None:
            #self.output('No results found')
            return

        # Use the returned Facebook ID to grab more info
        result = self.getUserDetailsFromId(id)
        
        # Parse the JSON response
        username = result.get('username')
        first_name = result.get('first_name')
        last_name = result.get('last_name')
        gender = result.get('gender')
        link = result.get('link')
        
        # Add the result to the table
        self.contacts.append([id,query,username,first_name,last_name,gender,link])
    
    # This function will take a phone number (supports a mask) and search for the user
    # The mask is in the form 555-555-**** and all *'s will be bruteforced
    # The mask can be any length in any position as long as there is only one mask and it is consecutive
    def phoneSearch(self, phone):
        # The logic in here may be hard to follow so I've commented it as
        # best as I possibly can.  Hope it makes sense!
    
        # Remove any formatting leaving just the numbers and mask
        phone = phone.translate(None, " ()-.")

        if len(phone) is not 10:
            self.error('Phone number should contain exactly 10 digits')
            return False

        if not '*' in phone:
            # Search one phone number and leave
            self.search(phone)
            return True

        # This is to ensure there is only one mask
        # and that the mask characters are all consecutive
        masklen = phone.count('*')
        check = (phone.rfind('*') - phone.find('*')) + 1

        if masklen is not check:
            self.error('Mask characters should be consecutive')
            return False

        # Replace the mask with a format specifier
        # eg. '****' will be '%04d'
        # eg. '***' will be '%03d'
        fmtString = phone.replace('*'*masklen, '%%0%sd' % masklen)

        # Brute force our missing digits 10^masklen
        for x in range(10**masklen):
            # Create our number to search for using the format specifier
            # to fill the string and left pad it with 0's to become a full 10 digits 
            searchNumber = fmtString % x
            self.search(searchNumber)
        return True
    
    def searchUrl(self, url):
        r = requests.get(url)
        return json.loads(r.content)

    def getUserDetailsFromId(self, id):
        url = self.GRAPH_URL + id
        results = self.searchUrl(url)
        return results

    def searchFirstResult(self, query):
        nextUrl = self.SEARCH_URL % {'query': query, 'token': self.access_token}
        self.output('searching: %s' % (query))
        self.verbose('URL: %s' % nextUrl)
        
        # Perform the search
        results = self.searchUrl(nextUrl)
        # Grab the data
        data = results.get('data')
        self.verbose('data: %s' % data)
        # Check for errors
        error = results.get('error')

        # Do we have an error?
        if error is not None:
            # If so grab the code
            code = error['code']
            # Is it because our access token expired
            if code is 190:
                # Do something useful here like prompt for a new token
                self.error('FB Token Error: %s' % (error['code']))
                return
            self.error('Error Code: %s' % (error['code']))
            self.error('Message   : %s' % (error['message']))

        # Check if we got any data
        if len(data) > 0:
            # Only interested in the first result
            first = data[0]
            id = first.get('id')
            return id